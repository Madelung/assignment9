package com.student.moritzmadelung.opengltest;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.opengl.GLES20;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Moritz Madelung on 24.11.2016.
 */
public class OpenGLShaderTest extends BaseOpenGLES20UnitTest {

    private final String validShaderCode =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    private final String invalidShaderCode =
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    "  syntax error" +
                    "}";

    public OpenGLShaderTest(){}

    public void test_compile_withValidCode_compiles() throws Throwable
    {
        runOnGLThread(new TestWrapper()
        {
            @Override
            public void executeTest() throws Throwable
            {
                int handle = ShaderUtils.compileShader(GLES20.GL_FRAGMENT_SHADER, validShaderCode);
                assertTrue(handle != 0);
                assertTrue(GLES20.glIsShader(handle));
            }
        });
    }

    public void test_compile_withInvalidCode_fails() throws Throwable
    {
        runOnGLThread(new TestWrapper()
        {
            @Override
            public void executeTest() throws Throwable
            {
                int handle = ShaderUtils.compileShader(GLES20.GL_FRAGMENT_SHADER, invalidShaderCode);
                assertTrue(handle == 0);
            }
        });
    }
}
