package com.student.moritzmadelung.opengltest;

import java.util.concurrent.CountDownLatch;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class BaseOpenGLES20UnitTest extends ActivityInstrumentationTestCase2<OpenGLES20UnitTestActivity>
{
    private OpenGLES20UnitTestActivity activity;

    public BaseOpenGLES20UnitTest()
    {
        super(OpenGLES20UnitTestActivity.class);
    }

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        activity = getActivity();
    }

    @Override
    public void tearDown() throws Exception
    {
        super.tearDown();

    }

    public void runOnGLThread(final TestWrapper test) throws Throwable
    {
        final CountDownLatch latch = new CountDownLatch(1);

        activity.getSurfaceView().queueEvent(new Runnable()
        {
            public void run()
            {
                test.executeWrapper();
                latch.countDown();
            }
        });

        latch.await();
        test.rethrowExceptions();
    }

    public static abstract class TestWrapper
    {
        private Error error = null;
        private Throwable throwable = null;

        public TestWrapper()
        {
        }

        public void executeWrapper()
        {
            try
            {
                executeTest();
            }
            catch (Error e)
            {
                synchronized (this)
                {
                    error = e;
                }
            }
            catch (Throwable t)
            {
                synchronized (this)
                {
                    throwable = t;
                }
            }
        }

        public void rethrowExceptions()
        {
            synchronized (this)
            {
                if (error != null)
                {
                    throw error;
                }

                if (throwable != null)
                {
                    throw new RuntimeException("Unexpected exception", throwable);
                }
            }
        }

        public abstract void executeTest() throws Throwable;
    }

}