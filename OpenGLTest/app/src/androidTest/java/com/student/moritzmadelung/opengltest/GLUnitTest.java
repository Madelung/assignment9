package com.student.moritzmadelung.opengltest;

import java.util.concurrent.CountDownLatch;

import android.app.Activity;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.test.ActivityInstrumentationTestCase2;

/**
 * <p>Extend ActivityInstrumentationTestCase2 for testing GL.  Subclasses can
 * use {@link #runOnGLThreadAndWait(Runnable)} Thread(Runnable)} and {@link #getGL()} to test from the
 * GL thread.</p>
 *
 * <p>Note: assumes a dummy activity, the test overrides the activity view and
 * renderer.  This framework is intended to test independent GL code.</p>
 *
 * @author Darrell Anderson
 */
public abstract class GLUnitTest<T extends Activity> extends ActivityInstrumentationTestCase2<T> {
    private final Object mLock = new Object();

    private Activity mActivity = null;
    private GLSurfaceView mGLSurfaceView = null;
    private GL10 mGL = null;

    // ------------------------------------------------------------
    // Expose GL context and GL thread.
    // ------------------------------------------------------------

    public GLSurfaceView getGLSurfaceView() {
        return mGLSurfaceView;
    }

    public GL10 getGL() {
        return mGL;
    }

    /**
     * Run on the GL thread.  Blocks until finished.
     */
    public void runOnGLThreadAndWait(final Runnable runnable) throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        mGLSurfaceView.queueEvent(new Runnable() {
            public void run() {
                runnable.run();
                latch.countDown();
            }
        });
        latch.await();  // wait for runnable to finish
    }

    // ------------------------------------------------------------
    // Normal users should not care about code below this point.
    // ------------------------------------------------------------

    public GLUnitTest(String pkg, Class<T> activityClass) {
        super(pkg, activityClass);
    }

    public GLUnitTest(Class<T> activityClass) {
        super(activityClass);
    }

    /**
     * Dummy renderer, exposes the GL context for {@link #getGL()}.
     */
    private class MockRenderer implements GLSurfaceView.Renderer {
        @Override
        public void onDrawFrame(GL10 gl) {
            ;
        }
        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height) {
            ;
        }
        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            synchronized(mLock) {
                mGL = gl;
                mLock.notifyAll();
            }
        }
    }

    /**
     * On the first call, set up the GL context.
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // If the activity hasn't changed since last setUp, assume the
        // surface is still there.
        final Activity activity = getActivity(); // launches activity
        if (activity == mActivity) {
            mGLSurfaceView.onResume();
            return;  // same activity, assume surface is still there
        }

        // New or different activity, set up for GL.
        mActivity = activity;
        mGLSurfaceView = new GLSurfaceView(activity);
        mGL = null;

        // Attach the renderer to the view, and the view to the activity.
        mGLSurfaceView.setRenderer(new MockRenderer());
        activity.runOnUiThread(new Runnable() {
            public void run() {
                activity.setContentView(mGLSurfaceView);
            }
        });

        // Wait for the renderer to get the GL context.
        synchronized(mLock) {
            while (mGL == null) {
                mLock.wait();
            }
        }
    }

    @Override
    protected void tearDown() throws Exception {
        mGLSurfaceView.onPause();
        super.tearDown();
    }

    public void runOnGLThread(final TestWrapper test) throws Throwable {
        final CountDownLatch latch = new CountDownLatch(1);
        mGLSurfaceView.queueEvent(new Runnable() {
            public void run()
            {
                test.executeWrapper();
                latch.countDown();
            }
        });
        latch.await();
        test.rethrowExceptions();
    }

    public static abstract class TestWrapper {
        private Error error = null;
        private Throwable throwable = null;

        public TestWrapper() {}

        public void executeWrapper() {
            try {
                executeTest();
            }
            catch (Error e) {
                synchronized (this) {
                    error = e;
                }
            }
            catch (Throwable t) {
                synchronized (this) {
                    throwable = t;
                }
            }
        }

        public void rethrowExceptions() {
            synchronized (this) {
                if (error != null) {
                    throw error;
                }

                if (throwable != null) {
                    throw new RuntimeException("Unexpected exception", throwable);
                }
            }
        }

        public abstract void executeTest() throws Throwable;
    }
}