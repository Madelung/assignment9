package com.student.moritzmadelung.opengltest;

import android.opengl.GLES20;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertTrue;

/**
 * Created by Moritz Madelung on 24.11.2016.
 */
@RunWith(JUnit4.class)
public class OpenGLShaderTryTest extends GLUnitTest {

    private final String validShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    private final String invalidShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  syntax error" +
                    "}";

    public OpenGLShaderTryTest(){
        super(OpenGLES20UnitTestActivity.class);}

    @Test
    public void test_compile_withValidCode_compiles() throws Throwable{
        runOnGLThread(new TestWrapper() {
            @Override
            public void executeTest() throws Throwable {
                int handle = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, validShaderCode);
                assertTrue(handle != 0);
                assertTrue(GLES20.glIsBuffer(handle));
            }
        });
    }

    @Test
    public void test_compile_withInvalidCode_fails() throws Throwable{
        runOnGLThread(new TestWrapper() {
            @Override
            public void executeTest() throws Throwable {
                int handle = MyGLRenderer.loadShader(GLES20.GL_FRAGMENT_SHADER, invalidShaderCode);
                assertTrue(handle == 0);
            }
        });
    }
}
