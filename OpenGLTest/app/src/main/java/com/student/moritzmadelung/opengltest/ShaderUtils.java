package com.student.moritzmadelung.opengltest;

import android.opengl.GLES20;
import android.util.Log;

/**
 * Created by Moritz Madelung on 28.11.2016.
 */

public class ShaderUtils {
        public static int compileShader(int type, String shaderCode)
        {
            int shaderHandle = GLES20.glCreateShader(type);
            GLES20.glShaderSource(shaderHandle, shaderCode);
            GLES20.glCompileShader(shaderHandle);

            int[] compileStatus = new int[1];
            GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileStatus, 0);

            if (compileStatus[0] == 0)
            {
                StringBuilder builder = new StringBuilder();
                builder.append("Error compiling ");
                builder.append(type == GLES20.GL_VERTEX_SHADER ? "vertex" : "fragment");
                builder.append(" shader: ");
                builder.append(GLES20.glGetShaderInfoLog(shaderHandle));
                Log.e("ShaderUtils", builder.toString());
                GLES20.glDeleteShader(shaderHandle);
                return 0;
            }

            return shaderHandle;
        }
}
