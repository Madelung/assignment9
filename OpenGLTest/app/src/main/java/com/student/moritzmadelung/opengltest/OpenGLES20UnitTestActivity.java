package com.student.moritzmadelung.opengltest;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Moritz Madelung on 23.11.2016.
 */

public class OpenGLES20UnitTestActivity extends Activity {

    private GLSurfaceView surfaceView;
    private CountDownLatch latch;

    public OpenGLES20UnitTestActivity () {
        latch = new CountDownLatch(1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        surfaceView = new GLSurfaceView(this);
        surfaceView.setEGLContextClientVersion(2);
        surfaceView.setRenderer(new EmptyRenderer(latch));
        setContentView(surfaceView);
    }

    @Override
    protected void onResume(){
        super.onResume();
        surfaceView.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
        surfaceView.onPause();
    }

    public GLSurfaceView getSurfaceView() throws InterruptedException {
        if (latch != null){
            latch.wait();
            latch = null;
        }
        return surfaceView;
    }

    private static class EmptyRenderer implements GLSurfaceView.Renderer {
        private CountDownLatch latch;

        public EmptyRenderer(CountDownLatch latch){
            this.latch = latch;
        }

        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config){}

        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height){}

        @Override
        public void onDrawFrame(GL10 gl){
            if(latch != null){
                latch.countDown();
                latch = null;
            }
        }
    }
}